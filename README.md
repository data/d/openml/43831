# OpenML dataset: Consumer-Price-Index

https://www.openml.org/d/43831

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Consumer Price Indices (CPI) measure changes over time in general level of prices of goods and services that households acquire for the purpose of consumption. CPI numbers are widely used as a macroeconomic indicator of inflation, as a tool by governments and central banks for inflation targeting and for monitoring price stability, and as deflators in the national accounts. CPI is also used for indexing dearness allowance to employees for increase in prices. CPI is therefore considered as one of the most important economic indicators. For construction of CPI numbers, two requisite components are weighting diagrams (consumption patterns) and price data collected at regular intervals. The data refers to group wise all India Consumer Price Index for Rural  Urban with base year 2010. The dataset is published by Central Statistical Office and released on 12th of every month.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43831) of an [OpenML dataset](https://www.openml.org/d/43831). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43831/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43831/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43831/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

